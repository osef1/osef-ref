# Cppcheck
Cppcheck is a static analysis tool for C/C++ code.
It provides unique code analysis to detect bugs and focuses on detecting undefined behaviour and dangerous coding constructs.
The goal is to have very few false positives.
Cppcheck is designed to be able to analyze your C/C++ code even if it has non-standard syntax (common in embedded projects). 

[[_TOC_]]

# Resources
* Official [site](http://cppcheck.net/)
* Wikipedia [page](https://www.wikiwand.com/en/Cppcheck)
* GitHub [repository](https://github.com/danmar/cppcheck/)
* SourceForge [repository](https://sourceforge.net/projects/cppcheck/)
* [Tutorial](https://github.com/khiguera/cppcheckTutorial)

# Install
````
sudo apt install cppcheck
````

# Build and install
````
git clone https://github.com/danmar/cppcheck.git
cd cppcheck
git checkout 2.5
mkdir build
cd build
cmake ..
cmake --build .
make install
````

## Update PATH
````
sudo nano /etc/profile
````
````
export PATH=/usr/local/bin/cppcheck:$PATH
````

## Install GUI
````
apt install qtbase5-dev qttools5-dev
cmake -DBUILD_GUI=ON ..
````

# Analyze local directory and sub-directories
````
cmake -DCMAKE_EXPORT_COMPILE_COMMANDS=ON
cppcheck --project=compile_commands.json
````

# One-liner
````
./analyze-cppcheck.sh [OPTIONS]
````

# Curated options
````
# [unusedFunction] forbids to build a library
# [missingIncludeSystem] not applicable as cppcheck cannot find system includes
# [unmatchedSuppression] displays unmatched suppressions
# [missingInclude] forbids to exclude googletest sources
````

# GitLab CI job
````
image: gcc

cppcheck:
  stage: build
  before_script:
    - apt update && apt -y install  cppcheck
  script:
    - ./analyze-cppcheck.sh [OPTIONS]
````
