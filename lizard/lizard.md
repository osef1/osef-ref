# Lizard

Lizard is an extensible Cyclomatic Complexity Analyzer for many programming languages including C/C++. It also does copy-paste detection  and many other forms of static code analysis.

[[_TOC_]]

# Resources
* Test [lizard online](http://www.lizard.ws/#)
* GitHub [repository](https://github.com/terryyin/lizard)
* Python [project](https://pypi.org/project/lizard/)

# Install
````
sudo apt install python3-pip
sudo pip3 install lizard --system
````

# Analysis
# Default local directory and sub-directories
````
lizard
````

# Curated one-liner
````
./analyze-lizard.sh [OPTIONS] [PATH]
````

# Curated options
* --languages cpp
  * Set analysis language to C++
* --modified
  * Use modified cyclomatic complexity (switch/case counts as one CCN)
* --sort cyclomatic_complexity
  * Sort warnings by cyclomatic complexity
* --CCN 8
  * Set function cyclomatic complexity theshold at 8
* --arguments 4
  * Set function parameters threshold at 4
* --Threshold nloc=70
  * Set function lines of code threshold at 70
* --length 80
  * Set function lines threshold at 80

# GitLab CI job
* Install pip3 (optional)
* Install lizard (optional)
* Run one-liner lizard analysis with project specific options and path

````
image: gcc

lizard:
  stage: build
  before_script:
    - apt update && apt -y install  python3-pip
    - pip3 install lizard
  script:
    - ./analyze-lizard.sh [OPTIONS] [PATH]
````
