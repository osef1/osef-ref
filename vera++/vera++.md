# Vera++

Vera++ is a programmable tool for verification, analysis and transformation of C++ source code.

[[_TOC_]]

# Resources
* BitBucket [wiki](https://bitbucket.org/verateam/vera/wiki/Home)
* GitHub [repository](https://github.com/verateam/vera)
* Debian [package](https://tracker.debian.org/pkg/vera++)

# [Build and install from source code](http://jurjenbokma.com/ApprenticesNotes/building_verapp_on_stretch.xhtml)
````
sudo apt install git cmake g++ tcl-dev tk-dev python-dev lua5.1 liblua5.1-dev libluabind-dev
git clone --recursive https://bitbucket.org/verateam/vera.git
cd vera
git checkout v1.3.0
mkdir build
cd build
cmake ..
make -j
make install
````

:information_source:  **Might fail because of Boost download limit**

:information_source:  Download archive from [new artifactory](https://boostorg.jfrog.io/native/main/release/1.69.0/source/boost_1_69_0.tar.bz2)

# Install Debian package
````
sudo apt install vera++
````

# Analyze local directory and sub-directories
````
vera++ *.h *.cpp */*.h */*.cpp
````

# One-liner
````
./analyze-vera++.sh [options] [list-of-files]
````

# Curated options
* --exclusions
  * Reference exclusions file
* --error
  * Non-zero exit code upon report
* --show-rule
  * Display rule name in report
* --summary
  * Display reports and files

# Curated exclusions
Exclusions are defined as REGEX and passed in a file since version 1.3.0

* L004: line is longer than 100 characters
  * Allow lines longer than 100 characters ...
* T003: keyword 'struct' not followed by a single space
  * Allow typedef struct
* T006: keyword 'delete' not immediately followed by a semicolon or a single space
  * Allow delete[]
* T013: no copyright notice found
  * Allow no copyright notice in each file

# GitLab CI job
* Run one-liner vera++ analysis with project specific options and files

````
image: gcc

analyze-vera++:
  stage: build
  script:
    - ./analyze-vera++.sh [options] [list-of-files]
````
