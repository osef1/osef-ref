# Cpplint

Cpplint is an open source lint-like tool developed by Google, designed to ensure that C++ code conforms to Google's coding style guides. 

[[_TOC_]]

# Resources
* Wikipedia [page](https://www.wikiwand.com/en/Cpplint)
* GitHub [repository](https://github.com/cpplint/cpplint)
* Python [project](https://pypi.org/project/cpplint/)

# Install
````
sudo apt install python3-pip
sudo pip3 install cpplint --system
````

# Analyze local directory and sub-directories
````
cpplint *.h *.cpp */*.h */*.cpp
````

# One-liner
````
./analyze-cpplint.sh [options] [file]
````

# Curated options
````
# build/header_guard should check uniqueness and not a cumbersome pattern
-build/header_guard

# runtime/references is discarded as passing a pointer is more prone to errors ...
-runtime/references

# runtime/string forbids static strings then decrease readability and runtime performances when string needs to be constructed from char*
-runtime/string

# runtime/arrays merely checks array size begin with letter k notit really is a constant ...
-runtime/arrays

# runtime/indentation_namespace forbids indentation in namespace which does not imprve readability
-runtime/indentation_namespace

# legal/copyright is redundant with repo LICENSE file
-legal/copyright

# whitespace/newline doesn't allow else statement as same level as corresponding if ...
-whitespace/newline

# whitespace/line_length 80 characters limit is obsolete for some time already ...
-whitespace/line_length

# whitespace/braces requires opening brace at end of line which does not improve readability
-whitespace/braces

# whitespace/parens forbids to align parenthesis vertically which does not improve readability
-whitespace/parens

# whitespace/indent doesn't support indent after #ifdef
-whitespace/indent

# readability/alt_tokens requires to replace operator not by ! which does not improve readability
-readability/alt_tokens
````

# GitLab CI job
* Install pip3 (optional)
* Install cpplint (optional)
* Run one-liner cpplint analysis with project specific options and file

````
image: gcc

cpplint:
  stage: build
  before_script:
    - apt update && apt -y install  python3-pip
    - pip3 install cpplint
  script:
    - ./analyze-cpplint.sh [options] [file]
````
