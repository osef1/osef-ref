#!/bin/bash

# Check a destination directory has been provided with Test subdirectory
if [ ! -z "$1" ]
then
    TEST_SUBDIR=$1/Test
    if [ -d "$TEST_SUBDIR" ]
    then
        cp test-gtest-xml.sh $TEST_SUBDIR
    else
        echo $TEST_SUBDIR is not a directory
    fi
else
    echo Please provide a destination directory
fi
