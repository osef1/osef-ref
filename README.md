# osef-ref

https://stackoverflow.com/questions/51582604/how-to-use-cpplint-code-style-checking-with-cmake

https://myopsblog.wordpress.com/2016/07/11/how-to-enable-multicast-on-linux-network-interface/

https://www.youtube.com/watch?v=rLopVhns4Zs&amp&t=77m13s

 We shouldn't define in a project what isn't a project requirement
 This means to pass cpplint executable location as cmake option
 Still cpplint will not parse parse .h in that case until project is modified accordingly ...

https://blog.kitware.com/static-checks-with-cmake-cdash-iwyu-clang-tidy-lwyu-cpplint-and-cppcheck/

## [Lizard](lizard/lizard.md)
## [Vera++](vera++/vera++.md)
## [Cpplint](cpplint/cpplint.md)
## [Cppcheck](cppcheck/cppcheck.md)
## [Scan-build](scan-build/scan-build.md)
## [Clang-tidy](clang-tidy/clang-tidy.md)

## gitlab runner
````
sudo apt install gitlab-runner
````

````
sudo gitlab-runner register
````

````
gitlab-runner exec shell build-pi
````

````
sudo cat /etc/gitlab-runner/config.toml
````

````
gitlab-runner unregister --url https://gitlab.com/ --token {TOKEN}
````

https://gitlab.com/fredloreaud/osef-posix/-/settings/ci_cd


https://www.devils-heaven.com/gitlab-runner-finally-use-your-raspberry-pi/


* Get user's group
    * groups username
* Add user to group
    * sudo usermod -G groupname username
* Delete user from group
    * sudo deluser username groupname
* Give root permissions to user
    * sudo usermod -a -G sudo username
    * sudo visudo
        * gitlab-runner ALL=(ALL) NOPASSWD: ALL
* Give root permissions to file
    * sudo chown root filename
    * sudo chmod u+s filename

## Vera++

http://jurjenbokma.com/ApprenticesNotes/building_verapp_on_stretch.xhtml

````
sudo apt install git cmake g++ tcl-dev tk-dev python-dev lua5.1 liblua5.1-dev libluabind-dev

git clone --recursive https://bitbucket.org/verateam/vera.git

cd vera

mkdir build

cd build

cmake ..

make -j
(might fail because of Boost download limit)

make install
````

## OCLint

### build

https://docs.oclint.org/en/stable/intro/build.html

sudo apt install ninja

git clone --recursive https://github.com/oclint/oclint.git

cd oclint/oclint-scripts

sudo ./make

### install

http://docs.oclint.org/en/stable/intro/installation.html

Download binaries from
https://github.com/oclint/oclint/releases

untar

cd into

cp bin/oclint* /usr/local/bin/
cp -rp lib/* /usr/local/lib/
