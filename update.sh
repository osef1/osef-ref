#!/bin/bash

install-tool()
{
    cd $1
        ./install.sh ../$2
    cd ..
}

update-tool()
{
    cd $1
        ./update.sh ../$2
    cd ..
}

# Check a destination directory has been provided
if [ ! -z "$1" ]
then
    if [ -d "$1" ]
    then
        install-tool clang-tidy $1
        install-tool cloc $1
        install-tool cmake $1
        install-tool cppcheck $1
        install-tool cpplint $1
        update-tool doxygen $1
        install-tool gcovr $1
        update-tool git $1
        #install-tool gtest $1
        install-tool iwyu $1
        install-tool lwyu $1
        install-tool lizard $1
        install-tool netbeans $1
        install-tool oclint $1
        install-tool scan-build $1
        install-tool vera++ $1
    else
        echo $1 is not a directory
    fi
else
    echo Please provide a destination directory
fi
