#!/bin/bash

# Check a destination directory has been provided
if [ ! -z "$1" ]
then
    if [ -d "$1" ]
    then
        cp osef.cmake $1
        cp build-debug.sh $1
        cp build-release.sh $1
    else
        echo $1 is not a directory
    fi
else
    echo Please provide a destination directory
fi
