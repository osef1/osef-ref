#!/bin/bash

mkdir -p build/doxygen

cd build/doxygen

cmake -DCMAKE_BUILD_TYPE=release ../..

make doc
