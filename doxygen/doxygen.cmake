# https://cmake.org/cmake/help/latest/module/FindDoxygen.html

# look for Doxygen package
find_package(Doxygen REQUIRED dot)

if(DOXYGEN_FOUND)

  set(DOXYGEN_EXCLUDE_PATTERNS */Test/* */googletest/* */NetBeans/*)
  set(DOXYGEN_SOURCE_BROWSER YES)

  # run "make doc" to generate Doxygen documentation
  doxygen_add_docs(
    doc 
    #${PROJECT_SOURCE_DIR} 
    ${SOURCES} 
    #File MsgQ 
    COMMENT "Generate Doxygen documentation."
  )

endif(DOXYGEN_FOUND)