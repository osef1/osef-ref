#!/bin/bash

# Check a destination directory has been provided
if [ ! -z "$1" ]
then
    if [ -d "$1" ]
    then
        cp doc-doxygen.sh $1
        cp doxyfile $1
    else
        echo $1 is not a directory
    fi
else
    echo Please provide a destination directory
fi
