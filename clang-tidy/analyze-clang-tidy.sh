#!/bin/bash

# https://clang.llvm.org/extra/clang-tidy/checks/list.html

CLANG_TIDY_CHECKS="*"

CLANG_TIDY_CHECKS+=",-altera-struct-pack-align" # enforce alignement optimization

CLANG_TIDY_CHECKS+=",-cppcoreguidelines-avoid-magic-numbers" # allow binary mask and literal offset (solved by constexpr?)
CLANG_TIDY_CHECKS+=",-cppcoreguidelines-pro-type-union-access" # allow signal action
CLANG_TIDY_CHECKS+=",-cppcoreguidelines-avoid-c-arrays" # allow c array
CLANG_TIDY_CHECKS+=",-cppcoreguidelines-pro-type-vararg" # allow message queue
CLANG_TIDY_CHECKS+=",-cppcoreguidelines-pro-type-reinterpret-cast" # allow reinterpret cast
CLANG_TIDY_CHECKS+=",-cppcoreguidelines-pro-type-cstyle-cast" # allow c-style cast
CLANG_TIDY_CHECKS+=",-cppcoreguidelines-pro-bounds-constant-array-index" # allow FD_SET
CLANG_TIDY_CHECKS+=",-cppcoreguidelines-avoid-non-const-global-variables" # allow private static attribute

CLANG_TIDY_CHECKS+=",-fuchsia-overloaded-operator" # allow operator overload
CLANG_TIDY_CHECKS+=",-fuchsia-default-arguments-declarations"
CLANG_TIDY_CHECKS+=",-fuchsia-default-arguments-calls"
CLANG_TIDY_CHECKS+=",-fuchsia-statically-constructed-objects" # allow static attribute
CLANG_TIDY_CHECKS+=",-fuchsia-default-arguments*" # necessary for clang-tidy older version on GitLab ...

CLANG_TIDY_CHECKS+=",-google-runtime-references"
CLANG_TIDY_CHECKS+=",-google-readability-casting" # allow c-style cast

# High Integrity C++ (https://www.wikiwand.com/en/High_Integrity_C%2B%2B)
CLANG_TIDY_CHECKS+=",-hicpp-vararg" # alias cppcoreguidelines-pro-type-vararg
CLANG_TIDY_CHECKS+=",-hicpp-signed-bitwise" # allow  FD_SET
CLANG_TIDY_CHECKS+=",-hicpp-no-assembler" # allow FD_SET
CLANG_TIDY_CHECKS+=",-hicpp-avoid-c-arrays" # alias cppcoreguidelines-avoid-c-arrays

CLANG_TIDY_CHECKS+=",-llvm-header-guard" # allow header guard not including absolute path
CLANG_TIDY_CHECKS+=",-llvm-include-order" # allow non-lexicographic include order
CLANG_TIDY_CHECKS+=",-llvmlibc-restrict-system-libc-headers" # allow system libc
CLANG_TIDY_CHECKS+=",-llvmlibc-implementation-in-namespace" # allow namespace other than __llvm_libc namespace
CLANG_TIDY_CHECKS+=",-llvmlibc-callee-namespace" # allow std namespace

CLANG_TIDY_CHECKS+=",-modernize-use-trailing-return-type" # allow non-auto return type
CLANG_TIDY_CHECKS+=",-modernize-use-using" # prompts gcc subobject-linkage warning
CLANG_TIDY_CHECKS+=",-modernize-avoid-c-arrays" # alias hicpp-avoid-c-arrays
CLANG_TIDY_CHECKS+=",-modernize-pass-by-value" # allow to pass parameter by reference

CLANG_TIDY_CHECKS+=",-readability-convert-member-functions-to-static"
CLANG_TIDY_CHECKS+=",-readability-magic-numbers" # alias cppcoreguidelines-avoid-magic-numbers
CLANG_TIDY_CHECKS+=",-readability-isolate-declaration" # allow FD_ZERO

mkdir -p build/release

cd build/release

clang-tidy --version

#cmake -DCMAKE_BUILD_TYPE=release "-DCMAKE_CXX_CLANG_TIDY=clang-tidy;-warnings-as-errors=*;-header-filter=.*;-line-filter=[{'name':'gtest.cc','lines':[[1,1]]}];-checks=$CLANG_TIDY_CHECKS" ../..
cmake -DCMAKE_BUILD_TYPE=release "-DCMAKE_CXX_CLANG_TIDY=clang-tidy;-warnings-as-errors=*;-header-filter=.*;-checks=$CLANG_TIDY_CHECKS" ../..
#cmake -DCMAKE_BUILD_TYPE=release "-DCMAKE_CXX_CLANG_TIDY=clang-tidy;-warnings-as-errors=*;-fix;-header-filter=.;-checks=$CLANG_TIDY_CHECKS" ../..
#cmake -DCMAKE_BUILD_TYPE=release -DCLANG_TIDY=ON ../..

make clean

make
