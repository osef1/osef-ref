# Clang-tidy
Clang-tidy is a C++ “linter” tool which purpose is to diagnose and fix typical programming errors.

[[_TOC_]]

# Resources
* Official [site](https://clang.llvm.org/extra/clang-tidy/)
* Checks [list](https://clang.llvm.org/extra/clang-tidy/checks/list.html)
* Raspberry Pi [installation](https://solarianprogrammer.com/2018/04/22/raspberry-pi-raspbian-install-clang-compile-cpp-17-programs/)

# Install
## Ubuntu x86
````
wget https://github.com/llvm/llvm-project/releases/download/llvmorg-13.0.0/clang+llvm-13.0.0-x86_64-linux-gnu-ubuntu-20.04.tar.xz
````
## Raspberry Pi
````
wget https://github.com/llvm/llvm-project/releases/download/llvmorg-13.0.0/clang+llvm-13.0.0-armv7a-linux-gnueabihf.tar.xz
````
## Common
````
tar -xvf [archive]
sudo mv [archive-folder] /usr/local/clang-13.0.0
rm [archive]
sudo nano /etc/profile
````
## Update PATH
````
export PATH=/usr/local/clang-13.0.0/bin:$PATH
export LD_LIBRARY_PATH=/usr/local/clang-13.0.0/lib:$LD_LIBRARY_PATH
````

# Analyze local directory and sub-directories
````
cmake "-DCMAKE_CXX_CLANG_TIDY=clang-tidy;-warnings-as-errors=*;-checks=*" .
make
````

# One-liner
````
./analyze-clang-tidy.sh
````

# Curated options
````
-altera-struct-pack-align" # enforce alignement optimization

-cppcoreguidelines-avoid-magic-numbers" # allow binary mask and literal offset (solved by constexpr?)
-cppcoreguidelines-pro-type-union-access" # allow signal action
-cppcoreguidelines-avoid-c-arrays" # allow c array
-cppcoreguidelines-pro-type-vararg" # allow message queue
-cppcoreguidelines-pro-type-reinterpret-cast" # allow reinterpret cast
-cppcoreguidelines-pro-type-cstyle-cast" # allow c-style cast
-cppcoreguidelines-pro-bounds-constant-array-index" # allow FD_SET
-cppcoreguidelines-avoid-non-const-global-variables" # allow private static attribute

-fuchsia-overloaded-operator" # allow operator overload
-fuchsia-default-arguments-declarations"
-fuchsia-default-arguments-calls"
-fuchsia-statically-constructed-objects" # allow static attribute
-fuchsia-default-arguments*" # necessary for clang-tidy older version on GitLab ...

-google-runtime-references"
-google-readability-casting" # allow c-style cast

# High Integrity C++ (https://www.wikiwand.com/en/High_Integrity_C%2B%2B)
-hicpp-vararg" # alias cppcoreguidelines-pro-type-vararg
-hicpp-signed-bitwise" # allow  FD_SET
-hicpp-no-assembler" # allow FD_SET
-hicpp-avoid-c-arrays" # alias cppcoreguidelines-avoid-c-arrays

-llvm-header-guard" # allow header guard not including absolute path
-llvm-include-order" # allow non-lexicographic include order
-llvmlibc-restrict-system-libc-headers" # allow system libc
-llvmlibc-implementation-in-namespace" # allow namespace other than __llvm_libc namespace
-llvmlibc-callee-namespace" # allow std namespace

-modernize-use-trailing-return-type" # allow non-auto return type
-modernize-use-using" # prompts gcc subobject-linkage warning
-modernize-avoid-c-arrays" # alias hicpp-avoid-c-arrays
-modernize-pass-by-value" # allow to pass parameter by reference

-readability-convert-member-functions-to-static"
-readability-magic-numbers" # alias cppcoreguidelines-avoid-magic-numbers
-readability-isolate-declaration" # allow FD_ZERO
````

# GitLab CI job
````
image: gcc

clang-tidy:
  stage: build
  script:
    - ./analyze-clang-tidy.sh
````
