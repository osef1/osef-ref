# Scan-build

[[_TOC_]]

# Resources
* Official [site](https://clang-analyzer.llvm.org/scan-build.html)
* Python [project](https://pypi.org/project/scan-build/)
* GitHub [repository](https://github.com/CAST-projects/scan-build)

# Install
See Clang-tidy [installation](https://gitlab.com/osef1/osef-ref/-/blob/master/clang-tidy/clang-tidy.md#install)

# Analyze local directory and sub-directories
````
````

# One-liner
````
````

# Curated options

# GitLab CI job
````
image: gcc

scan-build:
  stage: build
  script:
    - ./analyze-scan-build.sh
````
