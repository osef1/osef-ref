#!/bin/bash

# Check a destination directory has been provided
if [ ! -z "$1" ]
then
    if [ -d "$1" ]
    then
        ./update.sh $1
        cp git-get-repositories.sh $1 
    else
        echo $1 is not a directory
    fi
else
    echo Please provide a destination directory
fi
