#!/bin/bash

# get submodule
. ./git-get-repositories.sh

for URL in $SUBMODULES
do
    git submodule add $URL
done
