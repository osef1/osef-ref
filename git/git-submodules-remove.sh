#!/bin/bash

# get submodule
. ./git-get-repositories.sh

for URL in $SUBMODULES
do
    BASENAME=$(basename $URL)
    FILENAME=${BASENAME%.*}
    #EXTENSION=${BASENAME##*.}

    git submodule deinit -f $FILENAME
    git rm -f $FILENAME
    git rm --cached $FILENAME
    rm -rf .git/modules/$FILENAME
done

# URL https://gitlab.com/fredloreaud/osef-log
# BASENAME osef-log
# FILENAME osef-log
# EXTENSION osef-log

# URL git@gitlab.com:fredloreaud/osef-log.git
# BASENAME osef-log.git
# FILENAME osef-log
# EXTENSION git


