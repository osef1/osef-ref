#!/bin/bash

# Check a destination directory has been provided
if [ ! -z "$1" ]
then
    if [ -d "$1" ]
    then
        cp git-submodules-update.sh $1
        cp gitignore $1/.gitignore 
    else
        echo $1 is not a directory
    fi
else
    echo Please provide a destination directory
fi
